(ns clojurians-zulip.events.scraper
  (:import
   [java.time.format DateTimeFormatter]
   java.util.Locale
   [org.htmlcleaner HtmlCleaner DomSerializer CleanerProperties])
  (:require
   [clj-http.client :as client]
   [clj-xpath.core :as x :refer [$x:node* $x:text*]]
   [clojure.data.json :as json]
   [clojure.string :as str]
   [java-time :as jt]))


(defn html->xml
  [a-html-doc]
  (let [cleaner       (new HtmlCleaner)
        cleaner-props (new CleanerProperties)
        dom-srlzr     (new DomSerializer cleaner-props)
        cleaned-doc   (.clean cleaner a-html-doc)]
    (.createDOM dom-srlzr cleaned-doc)))


(defn url->xml [url]
  (let [headers {"User-Agent" "Mozilla/5.0 (Windows NT 6.1;) Gecko/20100101 Firefox/13.0.1"}]
    (-> url (client/get {:headers headers}) :body (html->xml))))


(defn- fetch-data [url extractor]
  (let [xml (url->xml url)]
    (reduce (fn [acc [k {:keys [xp process] :or {process #(-> % first (str/trim))}}]]
              (assoc acc k (->> xml ($x:text* xp) process)))
            {}
            extractor)))


(defn- scrape-dispatcher [url]
  (cond
    (re-seq #"meetup\.com" url)       :meetup
    (re-seq #"clojureverse\.org" url) :clojureverse
    :else                             :default))


(defmulti scrape #'scrape-dispatcher)

(defmethod scrape :meetup [url]
  (let [data-xp         "//script[@type='application/ld+json']"
        parse-data-date #(-> % jt/zoned-date-time jt/instant)
        get-data        #(-> %1
                             (->> (map json/read-str)
                                  (filter (fn [m] (contains? m "startDate"))))
                             first
                             (get %2))
        extractor       {:title       {:xp "//div[@id='page']//h1"}
                         :start       {:xp      data-xp
                                       :process #(-> %
                                                     (get-data "startDate")
                                                     parse-data-date
                                                     str)}
                         :till        {:xp      data-xp
                                       :process #(-> %
                                                     (get-data "endDate")
                                                     parse-data-date
                                                     str)}
                         :description {:xp "//h2[text()='Details']/parent::div/following-sibling::div[@class='break-words']"}
                         :online?     {:xp "//div[text()='Online event']" :process #(-> % first (and true))}}]
    (fetch-data url extractor)))

(defmethod scrape :clojureverse [url]
  (let [json-url (str url ".json")
        json     (json/read-str (slurp json-url) :key-fn #(keyword (str/replace % "_" "-")))]
    {:title       (get json :title)
     :description (first ($x:text* "/*" (html->xml (get-in json [:post-stream :posts 0 :cooked]))))
     :start       (get-in json [:event :start])}))

(defmethod scrape :default [_] {})


(comment
  (scrape "https://www.meetup.com/london-clojurians/events/292730420/")
  (scrape "https://www.meetup.com/London-Clojurians/events/293543779/")



  (jt/instant (jt/zoned-date-time (get (first (filter #(get % "startDate") (map json/read-str ($x:text* "//script[@type='application/ld+json']" (url->xml "https://www.meetup.com/london-clojurians/events/292730420/" #_"https://www.meetup.com/London-Clojurians/events/293543779/"))))) "startDate")))

  ($x:text* "//script" (html->xml (:description (scrape "https://clojureverse.org/t/visual-tools-meeting-12-data-rabbit/9268"))))

  #_(time->start&end (:time (scrape "https://www.meetup.com/london-clojurians/events/289598000/")))

  #_(time->start&end (:time (scrape "https://www.meetup.com/london-clojurians/events/288224866/")))

  (re-seq #"(?<= )\d" " 1")
  (let [time-str     "Friday, December 2, 2022, 9:30 AM to Saturday, December 3, 2022, 9:30 PM UTC"
        time-str "Tuesday, November 15, 2022 6:30 PM  to 8:30 PM UTC"
        pattern      #"(?x)
(?<=^| ) # preceded by start or a whitespace
\w+day,  # a weekday, e.g. 'Saturday'
.+?      # anything (non-greedy)
\d{4}    # year
"]
    #_(re-seq #"(?:^|to ).+? \d{4}" time-str)

    #_(re-seq #"(?<= |^)\w+day.+?\d{4}" time-str)
    (re-seq pattern time-str)
    (re-seq #"[^ ]+ [A|P]M" time-str))
  (:description (scrape "https://www.meetup.com/London-Clojurians/events/288373088/"))
  (def xml ())
  (require '[tick.core :as t])

  (t/in (t/now) "UTC")

  (t/in (jt/instant "2022-09-17T05:30:00+00:00") "UTC")
  (-> (t/now)
      (t/in "UTC"))

  (require '[clojure.data.json :as json])
  (def json (json/read-str (slurp "https://clojureverse.org/t/clojure-asia-september-2022-online-meetup/9346.json") :key-fn #(keyword (str/replace % "_" "-"))))
  json
  (jt/instant (get-in json [:event :start])))
