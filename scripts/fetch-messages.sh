#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail

case "${1:-}" in
    announcements)
        narrow='[{"operator": "stream", "operand":"announce"},{"operator":"topic", "operand":"announcements@slack"}]'
        num_before=30
        ;;
    events)
        narrow='[{"operator": "stream", "operand":"events"}]'
        num_before=1000
        ;;
    events-dev)
        narrow='[{"operator": "stream", "operand":"events-dev"}]'
        num_before=1000
        ;;
    *) echo "Choose one of 'announcements' or 'events'" && exit 1;;
esac

http --check-status --ignore-stdin --timeout=4 -a "${ZULIP_AUTH}" https://clojurians.zulipchat.com/api/v1/messages anchor==10000000000000000 num_before=="${num_before}" num_after==0 narrow=="${narrow}" apply_markdown==false
